> #  Pixel.JS

> ### Why did I make Pixel?

Pixel is a open-source search engine for files,torrents,videos to be honest any thing, Pixel is the same as any torrent site, but I plan to not 
get blocked, banned, or shutdown. 

Pixel was created orrginaly for me and a few freinds to share files, like torrents after our prefered torrent sites got shutdown, we both found it 
very useful, so I decided it would be a good idea to share it with the world, and hopefully you will all feal the same!

> ### About our Open-Sourceness 
we are open-source to the extent of the client, the server currently is not open-source, due to the early-stages of our encryption, we 
probably will never release our server for open-source, due to the multiple security threats that would come from doing so, if you would like to contribute 
to our server, please contact, @SirAlexLX on twitter. 




> #### What I am planning to do with Pixel 
>>
> + Add and manage your uploaded packages from Pixel.
> + Protected Downloads
> + Fast, secure, reliable Database.
> + Streamable downloads
> + Multiple Downloads
> + Unzip on request 
> + take inputs from terminal instead of through Pixel's Ui. //Commander power!
> + A buetiful Website, and useful docs. 
> + Support for custom Install-Scripts 
> #### Custom Install Scripts is a planned feature for Pixel, the ability to run Python, or Javascript scripts for installing 
Packages, or for managing packages, I.E Deleting, Starting, like Linux's "Service" command. 
> + Detailed and helpful Error messages.
> + Support for more archive types, Rar, 7zip, Gzip, etc. 
>>

> ## Pixel.JS setup.
Download latest build of Pixel.

Pixel can be started like any other node application, node Pixel.js/pixel (Current version is not global). 
 
 > ### ToDo! 
 
 > + Put installation check code into Pixel, instead of in a seperate install js script, I belive this will be more popular, will make installs easier.
 > + Code custom CDN server and add support for encrypted socket communications - the servers all talk to each other in encrypted sockets, so support is mandatory.
 > + Add the link addition system into the main file, using namespaces on the main server.
 
 
 > ### Requirements for links! 
 
 > + our download system can take either a, .../..zip URl, or a router to a CDN, like, http://example.com/cdn/foo, where foo, is the name of the file you want to be downloading.
 > + we request that you keep redirects to a mimimum, max of 2!
 
 
> ### If you want to host downloads, but on your servers. 

> + You can download our, CDN server, this will connect to a database, this can either be ours, or your own. 
> ## About our CDN server.
>  Our CDN server takes a request with an ID, say in your database you have ubuntu.iso file, with an id if 1155, 
inorder for the download link to be served, Pixel has to request the CDN server for it, with this URL, (we will use our server as an example)
http://cdn.pixel.io/1155 the server will look this up in the database, and return with, http://cdn.pixe.io/cdn/files/iso/ubuntu/version/1.5/ubuntu_1.5.iso, we do it this way
to save space in the database and make sorting and finding files easier, its easier to query the CDN server, then to go hunting through our many directorys to find a file.

>### Best hosting for a downloads or CDN server.

> + we host on a High bandwith Gigabyte connection, allowing us to deliver High speed, reliable, downloads and download uploads quickly, 
if you are hosting a server your self, we would appreciate it if you could host on a decent connection, to avoid slowing down the network. 
> + a lot more info about the CDN server will be in its README.

 > ### News! 
 
 > + None at the moment.
 
 > I am planning to make Pixel utilize nodes, to allow for a distrobuted network, with faster lookups and connections, quicker downloads, will also make 
 > the network harder to shutdown - as I am sure that will happen one day, if Pixel becomes popular.
 
 -Alex
